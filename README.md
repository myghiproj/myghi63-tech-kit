# Myghi63 Tech Kit

## What's all about?

This project is a work-in-progress, ArchISO-based live OS to diagnose, recover data and fix computers.

It will include lots of utilities, with XFCE desktop environment to be lightweight and friendly at the same time!
